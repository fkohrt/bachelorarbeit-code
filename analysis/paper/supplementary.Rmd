---
title: Replicating the natural selection of bad science
subtitle: Supplementary Material
author: Florian Kohrt
date: 2021-07-30
lang: en
bibliography_excluded: "`r here::here('analysis', 'assets', 'automated_assets', 'excluded.bib')`"
csl: "`r here::here('analysis', 'assets', 'automated_assets', 'apa.csl')`"
link-citations: yes
documentclass: scrartcl # use KOMA-Script's scrartcl for european typographic conventions
papersize: a4
graphics: yes # enable graphics if using knitr::include_graphics for LaTeX with custom template; see https://stackoverflow.com/a/28382257
csquotes: yes
mainfont: Libertinus Serif
mainfontoptions:
  - Extension=.otf
  - UprightFont=*-Regular
  - BoldFont=*-Bold
  - ItalicFont=*-Italic
  - BoldItalicFont=*-BoldItalic
sansfont: Libertinus Sans
sansfontoptions:
  - Extension=.otf
  - UprightFont=*-Regular
  - BoldFont=*-Bold
  - ItalicFont=*-Italic
monofont: DejaVu Sans Mono
monofontoptions:
  - Scale=0.83 # as suggested at https://tex.stackexchange.com/a/71346
mathfont: LibertinusMath-Regular
mathfontoptions: # unicode-math setting according to ISO 80000-2; source: https://github.com/pep-dortmund/toolbox-workshop/blob/29c4dd4/latex-template/header.tex#L36-L46
  - Extension=.otf
  - math-style=ISO
  - bold-style=ISO
  - sans-style=italic
  - nabla=upright
  - partial=upright
hyperrefoptions:
  - pdfprintscaling=None # disable print scaling; see https://tex.stackexchange.com/a/165348
output:
  bookdown::pdf_document2:
    extra_dependencies:
      csquotes: null # convert typewriter quotes into typographic quotes; use in conjunction with yaml metadata field "csquotes"; see https://pandoc.org/MANUAL.html#creating-a-pdf
      flafter: null # force floats forwards; see https://bookdown.org/yihui/rmarkdown-cookbook/figure-placement.html
      xspace: null # required by the typography.py filter
      placeins: section
    latex_engine: lualatex # lualatex: provides unicode and modern font file support; required by package selnolig
    pandoc_args:
      - --listings
      - !expr paste0("--filter=", here::here("analysis", "assets", "automated_assets", "typography.py"))
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "cito.lua"))
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "multiple-bibliographies.lua"))
    dev: "cairo_pdf"
    keep_tex: no # turn this on only when debugging
    keep_md: no # turn this on only when debugging
    includes:
      in_header:
        - !expr here::here("analysis", "assets", "manual_assets", "xmp.tex") # embed license information in PDF
        - !expr here::here("analysis", "assets", "manual_assets", "code_listing.tex")
        - !expr here::here("analysis", "assets", "manual_assets", "blockquote.tex")
    template: !expr here::here("analysis", "assets", "automated_assets", "default.latex")
  bookdown::html_document2:
    highlight: tango # for working line numbers highlight should not be "default" or "textmate"; see https://bookdown.org/yihui/rmarkdown-cookbook/number-lines.html
    self_contained: no
    global_numbering: true
    dev: svg
    pandoc_args:
      - !expr paste0("--filter=", here::here("analysis", "assets", "automated_assets", "typography.py"))
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "cito.lua"))
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "multiple-bibliographies.lua"))
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "pandoc-quotes.lua"))
  bookdown::odt_document2:
    global_numbering: true
    pandoc_args:
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "cito.lua"))
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "multiple-bibliographies.lua"))
  bookdown::word_document2:
    global_numbering: true
    pandoc_args:
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "cito.lua"))
      - !expr paste0("--lua-filter=", here::here("analysis", "assets", "automated_assets", "multiple-bibliographies.lua"))
nocite: '`r paste0("@", paste(seq_len(nrow(synthesisr::read_refs(filename = here::here("analysis", "assets", "automated_assets", "excluded.bib"), return_df = TRUE))), collapse = ", @"))`'
---

```{r setup, include = FALSE}
options(tinytex.clean = TRUE, # set this to FALSE only when debugging
        tinytex.verbose = FALSE) # set this to TRUE only when debugging
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE, error = FALSE, results = "hide",
                      tidy.opts = list(width.cutoff = 60), # break code listings
                      tidy = TRUE, # tidy = TRUE requires the formatR package
                      fig.align = "center")
knitr::opts_knit$set(progress = FALSE)
if (knitr:::pandoc_to() %in% c("odt", "docx"))
  knitr::opts_chunk$set(dev = "svg")

# prepare fonts
sysfonts::font_add("LibertinusSerif",
                   regular = "LibertinusSerif-Regular.otf",
                   italic = "LibertinusSerif-Italic.otf",
                   bold = "LibertinusSerif-Bold.otf",
                   bolditalic = "LibertinusSerif-BoldItalic.otf")
sysfonts::font_add("LibertinusSans",
                   regular = "LibertinusSans-Regular.otf",
                   italic = "LibertinusSans-Italic.otf",
                   bold = "LibertinusSans-Bold.otf")
showtext::showtext_auto()

# prepare ggplot2
ggplot2::theme_set(ggplot2::theme_bw(base_family = "LibertinusSerif"))
allCategories <- c("effort", "power", "replicationRate", "alpha", "falseDiscoveryRate")

library(targets)
source(here::here("R", "functions.R"))
```

# Toolkits

```{r toolkits-full-table-latex, results = "asis", eval = knitr:::is_latex_output()}
startRow <- 1
remainingRows <- nrow(tar_read(toolkitsDF))

while (remainingRows > 0) {
  newDF <- tar_read(toolkitsDF)[seq_len(min(remainingRows, 30)) + startRow - 1, ]
  row.names(newDF) <- NULL
  print(kableExtra::column_spec(kableExtra::kable_styling(knitr::kable(newDF, "latex", booktabs = TRUE, escape = TRUE), full_width = FALSE, latex_options = c("striped", "scale_down")), 1, width = "5em"))
  startRow <- startRow + 30
  remainingRows <- remainingRows - 30
}
```

```{r toolkits-full-table-other, results = "markup", eval = !knitr:::is_latex_output()}
reactable::reactable(tar_read(toolkitsDF), searchable = TRUE, showPageSizeOptions = TRUE)
```

# Excluded studies

::: {#refs_excluded}
:::

# About this document

Created on `r withr::with_locale(new = c("LC_TIME" = "en_US.UTF-8"), code = format(Sys.time(), format='%B %e %Y, %R', tz='Europe/Berlin'))`.

```{r license-icon, out.width = "0.806in", results = "asis", fig.pos = "!H"}
# add textual visual license info
# for 403 px (CC icon width) and 500 dpi: out.width = px/dpi = 0.806 in
cat(getAttributionString(author = rmarkdown::metadata$author, html = knitr:::is_html_output()))
if (knitr:::is_latex_output()) {
  knitr::include_graphics(tar_read(license_icon_pdf))
} else if (knitr:::is_html_output()) {
  cat(paste0('<a rel="noreferrer" href="', getLicenseData()["url_deed"], '"><img alt="Creative Commons Lizenzvertrag ', getLicenseData()["plain"], '" style="border-width:0" src="', fs::path_rel(tar_read(license_icon_svg), start = here::here("analysis", "paper")), '" /></a>'))
} else {
  knitr::include_graphics(tar_read(license_icon_png))
}
```
