tar_target(power-assumption-positive-data, {
  posNegVals <- labEvolution::getConfigMatrix("posNegVals")
  posNegVals["novelPublicationProbability", "positive"] <- 0.75
  posNegVals["novelPublicationProbability", "negative"] <- 0.5
  confDisconfVals <- labEvolution::getConfigMatrix("confDisconfVals")
  confDisconfVals["replicationPublicationProbability", "confirmation"] <- 0.75
  confDisconfVals["replicationPublicationProbability", "disconfirmation"] <- 0.75
  
  results <- list()
  for (i in seq_len(50)) {
    result <- labEvolution::badScience.power(stepQuantity = 500000, posNegVals = posNegVals, confDisconfVals = confDisconfVals, prngSeed = 314159L + i)
    # append to list, see https://stackoverflow.com/a/13765279
    results <- unlist(list(results, list(result)), recursive = FALSE)
  }
  results
})
