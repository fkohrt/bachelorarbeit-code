tar_target(payOffDistribution-exploration-data, {
  labParameters <- labEvolution::getConfigMatrix("labParameters")
  labParameters["mutationProbability", "replicationRate"] <- 0
  labParameters["mutationProbability", "effort"] <- 0
  labParameters["mutationProbability", "power"] <- 0
  
  labQuantity <- 100
  initialValues <- labEvolution::getConfigMatrix("initialValues", labQuantity = labQuantity)
  initialValues[seq_len(labQuantity/2), "effort"] <- 15
  initialValues[seq_len(labQuantity/2)+(labQuantity/2), "effort"] <- 75
  
  result <- list(matrix(ncol = 5, nrow = 0),
                 matrix(ncol = 5, nrow = 0),
                 matrix(ncol = 5, nrow = 0))
  for (i in seq_len(50)) {
    temp <- list()
    for (replicationRate in c(0.1, 0.25, 0.5)) {
      labParameters["initialValue", "replicationRate"] <- replicationRate
      tempInner <- labEvolution::simulate(labQuantity = labQuantity,
                         labParameters = labParameters,
                         initialValues = initialValues,
                         stepQuantity = 110,
                         noReplicationStepQuantity = 10,
                         datapointQuantity = 1,
                         evolution = FALSE,
                         output = "labs",
                         prngSeed = 314159L + i,
                         verbosity = 0)
      # append to list, see https://stackoverflow.com/a/13765279
      temp <- unlist(list(temp, list(tempInner)), recursive = FALSE)
    }
    result <- list(rbind(result[[1]], temp[[1]]),
                   rbind(result[[2]], temp[[2]]),
                   rbind(result[[3]], temp[[3]]))
  }
  result
})
