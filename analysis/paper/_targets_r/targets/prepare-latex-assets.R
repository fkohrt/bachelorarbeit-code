list(
  tar_target(
    template_tex_original,
    dl("https://github.com/jgm/pandoc-templates/raw/614de99/default.latex",
       file.path("..", "assets", "automated_assets"),
       name = "default.latex.original"),
    format = "file"
  ),
  tar_target(
    template_tex_patch,
    file.path("..", "assets", "manual_assets", "default.latex.patch"),
    format = "file"
  ),
  tar_target(
    template_tex,
    fs::path_rel(path = patch(template_tex_original, template_tex_patch, here::here("analysis", "assets", "automated_assets", "default.latex")), start = here::here("analysis", "paper")),
    format = "file"
  ),
  tar_target(
    xmp_metadata,
    writeText(getXMPMetadata(title = rmarkdown::metadata$title,
                             author = rmarkdown::metadata$author,
                             work_url = "https://gitlab.com/fkohrt/bachelorarbeit-code"),
              file = file.path("..", "assets", "automated_assets", "metadata.xmp")),
    format = "file"
  ),
  tar_target(
    titlepage_tex,
    file.path("..", "assets", "manual_assets", "titlepage.tex"),
    format = "file"
  ),
  tar_target(
    xmp_tex,
    file.path("..", "assets", "manual_assets", "xmp.tex"),
    format = "file"
  ),
  tar_target(
    blockquote_tex,
    file.path("..", "assets", "manual_assets", "blockquote.tex"),
    format = "file"
  ),
  tar_target(
    codelisting_tex,
    file.path("..", "assets", "manual_assets", "code_listing.tex"),
    format = "file"
  ),
  tar_target(
    font_libertinus,
    dlLibertinusFont("."),
    format = "file"
  ),
  tar_target(
    license_icon_pdf, {
      file <- ""
      icons <- getLicenseIconURLs()
      if ("eps" %in% names(icons))
        file <- dl(icons["eps"], file.path("..", "assets", "automated_assets"))
      else {
        file <- svgToPDF(dl(icons["svg"], file.path("..", "assets", "automated_assets")))
      }
      file
    },
    format = "file"
  )
)
