tar_target(payOffDistribution-data, {
  result <- list(matrix(ncol = 5, nrow = 0),
                 matrix(ncol = 5, nrow = 0),
                 matrix(ncol = 5, nrow = 0))
  for (i in seq_len(50)) {
    temp <- labEvolution::badScience.payOffDistribution(prngSeed = 314159L + i, verbosity = 0, conformance = "conceptual")
    result <- list(rbind(result[[1]], temp[[1]]),
                   rbind(result[[2]], temp[[2]]),
                   rbind(result[[3]], temp[[3]]))
  }
  result
})
