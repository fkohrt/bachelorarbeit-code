tar_target(effortReplicationRate-implemented-data, {
  results <- list()
  for (i in seq_len(50)) {
    result <- labEvolution::badScience.effortReplicationRate(conformance = "implemented", prngSeed = 314159L + i)
    # append to list, see https://stackoverflow.com/a/13765279
    results <- unlist(list(results, list(result)), recursive = FALSE)
  }
  results
})
