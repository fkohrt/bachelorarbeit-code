Creative Commons Trademark Policy (7 June 2021)

Our trademarks: Trademarks are words, graphic designs, or other indicia
that identify the source of a product or service. Creative Commons uses
a variety of trademarks. Some Creative Commons trademarks serve the
purpose of (i) communicating the type of legal tool chosen by the rights
holder and/or (ii) indicating that the creator has applied a Creative
Commons license to her work. Our registered trademarks and other
trademarks include CREATIVE COMMONS (regardless of stylization,
capitalization, translation, or other presentation), CC (including the
CC in a circle logo (the “CC Logo”) and CC standing alone), CC+ (within
a circle or standing alone) and CCPlus, CC0, all of the Creative Commons
license and public domain buttons and icons, and any combination of the
foregoing, whether integrated into a larger whole or standing alone.
This also includes all of the CC trademarks incorporated into Unicode
and any other similar standard.

How you can use CC trademarks:

CC’s trademarks are not licensed under a Creative Commons license. You
are authorized to use our trademarks subject to this Trademark Policy,
and only on the further condition that you download images of the
trademarks directly from our website or apply them through software that
incorporates the trademarks in Unicode. You are not authorized to use
any modified versions of our trademarks, except that you may use a
different color for the CC logo and its background so long as the two
colors chosen have a contrast ratio of at least 3:1.

Additional specific rules for particular trademarks: 

Creative Commons Public Copyright License Marks: Creative Commons
licenses the use of its public copyright license marks, which include
the CC Logo, on the conditions that you use the marks solely to describe
the Creative Commons license that applies to a particular work and, in a
manner reasonable to the medium and context, include the URI or a
hyperlink to the corresponding Commons deed on the Creative Commons
server.

Public Domain Dedication Marks: Creative Commons licenses the use of its
public domain dedication marks, on the conditions that you use the mark
solely to describe that the CC0 Public Domain Dedication applies to a
particular work and, in a manner reasonable to the medium and context,
include the URI or a hyperlink to the Commons deed on the Creative
Commons server.

Public Domain Mark: Creative Commons licenses the use of its trademarked
Public Domain Mark badge on the conditions that you use the mark solely
to describe that the Creative Commons Public Domain Mark applies to a
particular work and, in a manner reasonable to the medium and context,
include the URI or hyperlink to the Public Domain Mark on the Creative
Commons server.

Open COVID Pledge: Creative Commons licenses the use of its Open COVID
Pledge marks as set forth at https://opencovidpledge.org/terms-of-use/.

Creative Commons License Buttons and Icons: Creative Commons licenses
the use of its button marks that describe a particular legal tool and
its icon marks that describe a key license element, such as BY, NC, ND,
and SA, on the conditions that you use the mark solely to describe the
Creative Commons legal tool that applies to a particular work and, in a
manner reasonable to the medium and context, include the URI or
hyperlink to the relevant Commons deed on the Creative Commons server.

Legacy marks: Creative Commons has retired some of its prior legal
tools, all of which have associated trademarks, such as the Developing
Nations License, the Sampling License, and Founder’s Copyright. Although
these tools have been retired and are no longer recommended for use,
they are still legally effective as to works to which they are applied.
Therefore, those trademarks may only be used under the terms and
conditions of this Trademark Policy. Creative Commons licenses the use
of its legacy marks on the conditions that you use the mark solely to
describe the Creative Commons legal tool that applies to the particular
work and, in a manner reasonable to the medium and context, include the
URI or hyperlink to the relevant Commons deed on the Creative Commons
server. Note that legacy marks are not available for download on our
website.

Specific rules for particular uses of trademarks: 

Promotional Uses: If you would like to use the Creative Commons or other
CC trademarks on event titles, product names, merchandise, and other
promotional uses (e.g., “Creative Commons Film Festival”), you must
first receive permission from Creative Commons. Please submit your
request to legal@creativecommons.org with a description of your event or
product, and an explanation as to how Creative Commons, CC licensing,
and/or CC-licensed works are integral to it.

Domain Names: Because of the likelihood of confusion, using the Creative
Commons name in a domain name is prohibited without express permission
from Creative Commons.

Referential uses: For the avoidance of doubt, you do not need our
permission to use our corporate logo for referential use (e.g., to refer
to Creative Commons as an organization), provided that such use does not
imply endorsement by or association with Creative Commons.

Descriptive uses: For the avoidance of doubt, you do not need our
permission to use the license buttons and icons, public domain
dedication marks, and other trademarks for descriptive use (e.g., to
describe CC licensing in explanatory materials), provided that such use
does not imply endorsement by or association with Creative Commons.

License Modification:

To prevent confusion and maintain consistency, you are not allowed to
use CREATIVE COMMONS, CC, the CC Logo, or any other Creative Commons
trademarks with modified versions of any of our legal tools or Commons
deeds. Specifically:

1.  If you make a change to the text of any CC license, you may no
    longer refer to it as a Creative Commons or CC license, and you must
    not use any CC trademarks (including the Creative Commons name) or
    branding in connection with the license. For the avoidance of doubt,
    this includes translations of CC licenses that have not been made
    and approved by CC in accordance with the Legal Code Translation
    Policy.
2.  If you place any restriction on use of a CC-licensed work that has
    the effect of limiting or further conditioning the permissions
    granted to the public under the standard CC license, you must not
    use any CC trademarks or branding in connection with the license or
    in any way that suggests the work is available under a CC license.
    These restrictions often appear in terms of use on websites where
    CC-licensed content is hosted, or as part of terms for downloading
    CC-licensed content.

Right to revoke:

Creative Commons retains the right to revoke any trademark license for
any reason or for no specified reason. Creative Commons is particularly
likely to revoke a license if, in its sole discretion, it finds that
your use of the trademark is likely to bring disrepute to Creative
Commons or any of its trademarks, or confuses the public.

Additional trademark permissions:

In addition to the trademark permissions granted in advance to the
public as set forth above, Creative Commons may agree to grant
additional permissions upon request. Please submit any such request to
legal@creativecommons.org.

  [CC’s trademarks are not licensed under a Creative Commons license]: https://creativecommons.org/faq/#could-i-use-a-cc-license-to-share-my-logo-or-trademark
  [Legal Code Translation Policy]: https://wiki.creativecommons.org/Legal_Code_Translation_Policy