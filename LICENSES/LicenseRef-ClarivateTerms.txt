Clarivate Terms
These Terms govern your use of the Clarivate products and services that you access through our platform(s),
website(s) or are otherwise identified in your order form, statement of work or other ordering document (collectively
“order form”). “We”, “our” and “Clarivate” means the Clarivate entity identified in the order form; “you” and “your”
means the Client entity identified in the order form.
The order form, any applicable referenced documents (such as specific product/service terms and operational
materials), as updated by us from time to time and these Terms constitute the complete agreement between us
("the agreement"), and supersede any prior discussions or representations regarding your order, unless fraudulent.
Other terms and conditions you seek to incorporate in any purchase order or otherwise, even where such document
is signed by us as a courtesy, are excluded, and your use of the products confirms your acceptance of these Terms.

1. Our products and services
(a) Order form. Your order form identifies the products, services, deliverables and/or data you are purchasing or
subscribing to (the "products"), and the quantities, relevant restrictions, charges and other details of your order
(including any referenced documents which may apply to those products).
(b) Intellectual Property. Together with our licensors, we retain all ownership of and all rights in our products
(including any underlying data model, database or data set) and any configurations or modifications to our products
("our property"). Our property constitutes our valuable intellectual property, confidential information and trade
secrets, and you may only use it as expressly permitted in the agreement. You must promptly notify us if you become
aware of any unauthorized use of our property.
(c) Compliance. Each of us shall at all times act in accordance with applicable laws, rules, regulations, export controls
and economic sanctions as they apply to each of us in connection with our provision and your use of our products
("applicable laws").
(d) Updates. Our products change from time to time. If we fundamentally change the products in a way which
impacts your usage of the products, you may terminate the affected products on written notice no later than 30 days
after the change.
(e) Passwords. Your access to certain products is password protected. Sharing passwords is strictly prohibited. Each of
us shall maintain industry standard computing environments to ensure that our property is secure and inaccessible to
unauthorized persons.
(g) Usage information. We may collect information related to your use of our products. We may use this information
for legitimate business reasons including without limitation to recommend products, services or functionality that
may interest users, to test and improve our products and to protect and enforce our rights under the agreement, and
may pass this information to our third party providers for the same purposes.
(h) Feedback and knowledge. Where you provide any comments, recommendation, suggestion or ideas, or any other
feedback related to our products, data or services ("feedback") we may use and exploit such feedback without
restriction or obligation to you and you will not obtain any rights in our products, data or services. We may freely use
our general knowledge, skills and experience, and any ideas, concepts, processes, know-how and techniques
developed by us while providing any products (including professional services), provided we do not use your
confidential or other proprietary information.
(i) Documentation. You may print or download PDF copies of our documentation for your internal use with the
products, provided all copyright or proprietary rights notices are retained.
(j) Third party providers. Our products may include data, software and services from third parties. Some third party
providers require us to pass additional terms through to you, and you must comply with these additional terms as
applicable. The third party providers change their terms occasionally and new third party providers are added from
time to time. To see the current third party additional terms that apply to your use of our products visit
https://clarivate.com/terms-of-business.

CLARIVATE ANALYTICS | TERMS

PAGE 1

2. Your obligations
(a) Limited license. You may only use the products in accordance with the applicable license set out in sections 3 to 6
and the relevant product/service terms referenced on the order form. You are responsible for all acts or omissions of
your users in connection with our products, and ensuring users comply with these terms.
(b) Your content. You retain ownership of your pre-existing content, data and materials that you provide to us, or use
with the products ("your content"). You hereby grant us a license to use your content as required by us to provide
you with the products (including to sublicense the same to our subcontractors, as required). You must (i) ensure your
content does not infringe third party rights or any applicable laws; and (ii) notify us in advance before transmitting to
us, and clearly mark, any of your content that contains sensitive data, including the jurisdiction and classification
under applicable export control laws. Sensitive data may include any information, data, or source code that is on an
export controls list or equivalent list of any applicable jurisdiction or that is related to weapons, military/defense,
intelligence, or law enforcement; aerospace or subsea technologies; cryptography, encryption, or cybersecurity tools;
advanced or cutting-edge items or technologies; or items that could pose a danger to health or safety. We disclaim
all responsibility for backing up your content.
(c) General obligations. You must (i) ensure we have up-to-date contact and billing information for your order; (ii)
provide detailed, accurate and sufficiently complete information, specifications and instructions in a timely manner;
(iii) ensure you are permitted to allow us to use and modify your equipment, systems, software and content, as
required to provide our products; and (iv) perform any additional obligations specified in your order form. If
reasonably requested, you must make authorized personnel available to agree on the impact of any failure or delay
by you to comply with these requirements, and you must not unreasonably withhold or delay your consent to any
consequential changes to the agreement.
(d) Third-party technology. You may only integrate our products with third-party technology where expressly
approved by us in writing, or where expressly permitted in the relevant product/service terms, and you are
responsible for procuring, maintaining and complying with any necessary license for the third party technology (which
is independent of the agreement and your license to our products).
(e) Unauthorized technology. Unless expressly permitted elsewhere in the agreement for the relevant product, you
must not (i) introduce any malicious software into our property or network; (ii) run or install any computer software
or hardware on our products or network; (iii) download or scrape data from our products; (iv) perform any text or
data mining or indexing of our products or any underlying data; (v) use our products or underlying data in conjunction
with any third-party technology or any artificial intelligence, algorithms or models; or (vi) use our products or
underlying data to develop or train any artificial intelligence, algorithms or models.
(f) Limitations. Unless expressly permitted elsewhere in the agreement, you may only use the products for your
internal business purposes and may not: (i) sell, sublicense, distribute, display, store, copy, modify, decompile or
disassemble, transform, reverse engineer, benchmark, frame, mirror, translate or transfer our property in whole or in
part, or as a component of any other product, service or material; (ii) use our property to create any derivative works
or any products (including tools, algorithms or models) that compete with or provide a substitute for a product
offered by Clarivate or its third party providers; or (iii) allow any third parties or unauthorized users to access, use or
benefit from our property in any way whatsoever. In each case, exercising legal rights that cannot be limited by
agreement is not precluded.
(g) Your Responsibilities. You are responsible for any violation of applicable laws or regulation, or violation of our or
any third party rights (including unauthorized use) related to (i) your content or your instructions to us; (ii) your
combination or modification of our products or other property, or use with any other materials; (iii) your failure to
install updates we have provided to you; or (iv) your breach of the agreement. You are also responsible for claims
brought by third parties receiving the benefit of our products through you. If you use our products in breach of
Sections 2 (e) or (f) you must delete or destroy any infringing material on our request. You must reimburse us if we
incur costs or suffer losses in the circumstances set out in this clause.

3. Information services
(a) License. Your permitted users may, for your internal business purposes only, view, download and print reasonable
amounts of the data for their own individual use. We determine a “reasonable amount” of data by comparing a user’s
activity against the average activity rates for all other users of the same product.

CLARIVATE ANALYTICS | TERMS

PAGE 2

(b) Distribution. Users may on an infrequent, irregular and ad hoc basis, distribute limited extracts of our data as
incidental samples or for illustrative or demonstration purposes in reports or other documentation created in the
ordinary course of their role. We determine a ‘limited extract’ as an amount of data that has no independent
commercial value and could not be used as a substitute for any service (or a substantial part of it) provided by us, our
affiliates or third party providers. Data may also be distributed: (i) amongst authorized users; (ii) to government and
regulatory authorities investigating you, if specifically requested; and (iii) to persons acting on your behalf, to the
extent required to provide legal or financial advice to you, provided they are not competitors of Clarivate.
(c) Attribution. As reasonably required for these purposes, users may quote and excerpt our data in their work,
provided they appropriately cite and credit Clarivate as the source.

4. Installed software
(a) License. You may install our software and documentation only for your own internal business purposes. Software
licenses do not include updates (bug fixes, patches, maintenance releases), upgrades (releases or versions that
include new features or additional functionality) or APIs unless expressly stated in the order form. Your order form
details your permitted installations, users, locations, the specified operating environment and other permissions and
restrictions. You may use our software in object code only. You are responsible for backups and may only make
necessary copies of our software for such purposes.
(b) Delivery. We deliver our software by making it available for download. You may first need to provide us with
certain identifying information about your system administrator and you may be required to confirm availability or
installation of our software.
(c) Acceptance. Unless set forth otherwise in an order form, when you download our software and documentation,
you are accepting it for use in accordance with the agreement.

5. Clarivate hosted software
(a) License. You may use our hosted software only for your own internal business purposes. Your order form details
your permitted users, locations and other permissions and restrictions.
(b) Delivery. We deliver our hosted software by providing you with online access to it. Unless set forth otherwise in
an order form, when you access our hosted software, you are accepting it for use in accordance with the agreement.
(c) Content. You grant us permission to use, store and process your content in accordance with applicable law. Access
and use of your content by us, our employees and contractors will be directed by you and limited to the extent
necessary to deliver the hosted software, including training, research assistance, technical support and other services.
We will not disclose your content except to support the hosted software, unless required by applicable laws (when
we will use our reasonable efforts to provide notice to you). We may delete or disable your content if required under
applicable laws or where such content violates the agreement (and we will use our reasonable efforts to provide
notice to you of such action). You may export your content prior to termination or, where content cannot be
exported and is accessible by us, we may, at your cost and upon execution of an order form for such services, provide
you with a copy of such content.
(d) Security. We will inform you in accordance with applicable laws if we become aware of any unauthorized third
party access to your content and will use reasonable efforts to remedy identified security vulnerabilities. Our hosted
software is designed to protect your content, however you are responsible for maintaining backups of your content. If
your content is lost or damaged due to our breach, we will assist you in restoring your content to the hosted software
from your last available back up copy.

6. Professional services
(a) License. Unless otherwise set out in the order form, you will own the deliverables set out in the order form,
provided that (i) we retain all intellectual property rights in and to any pre-existing products, data, codes, content,
methodologies, services, templates, tools or other materials incorporated into the deliverables ("Clarivate IP") and
you receive a license to use the Clarivate IP solely to the extent necessary to utilize the deliverables for your internal
business purposes; and (ii) if the deliverables include any configurations or modifications to our pre-existing products
(including but not limited to curated data sets) we retain all intellectual property rights in and to such deliverables,

CLARIVATE ANALYTICS | TERMS

PAGE 3

and you receive a license to use them in the same way as you are licensed to use the relevant product under these
Terms. You agree deliverables are deemed accepted upon delivery unless agreed otherwise in an order form.
(b) Changes. Either of us may make written (including email) requests to change any aspect of the professional
services, provided that no change will take effect unless and until we have each signed a formal change order setting
out the impact of the change and any consequential changes required to the agreement. Neither of us will
unreasonably withhold our agreement to a change.
(c) Access. As required for us to perform the relevant services, you must provide reasonable access to your sites,
equipment and systems and ensure the health and safety of our personnel on your premises and full cooperation
from your qualified and experienced personnel as reasonably required. We will take reasonable steps to ensure that
while on your site our personnel comply with reasonable security, health and safety and confidentiality requirements
that are notified to us in advance.

7. APIs and Data Feeds
Where we make our data available to you via API or a data feed, the information service terms (section 3 above)
apply to the data you receive. You must ensure that the data remains behind your firewall and is only accessible to
your internal users. If we deliver data via a data feed, you are responsible for loading and maintaining data in a timely
manner into your data stores. If we make an API available to you, you may use our APIs to enable authorized users to
use our products in accordance with the agreement in conjunction with your own technology systems provided
Clarivate approved accreditations remain visible at all times. Our API/data feed keys must not be: (i) shared in any
way; (ii) used for multiple interfaces; or (iii) used to create products or services detrimental to Clarivate, our affiliates
or third party providers. You must demonstrate interfaced systems if reasonably requested by us.

8. Charges
(a) Payment and taxes. You must pay our charges and reasonable expenses, together with any applicable taxes,
without deduction within 30 days of the date of invoice, unless otherwise provided on your order form. Payment
must be in the currency stated on your order form. We may levy a service charge of 1% per month or the highest
lawful interest rate (whichever is lower) for late payment plus our reasonable collection costs, including attorneys’
fees. Our fees are exclusive of tax, and shall be paid by you free and clear of all deductions or witholdings provided, if
you are required by law to deduct or withhold you will be responsible for paying to us such additional amount as will,
after such deduction or witholding has been made, leave us with the same amount as we would have been entitled to
receive in the absence of any such requirement to make a deduction or withholding. Invoice disputes must be
notified within 15 days. Once resolved, payment of disputed invoices will be due immediately.
(b) Changes. We may change the charges for our products with effect from the start of each renewal term by giving
you at least 60 days’ written notice. If we believe your creditworthiness has deteriorated we may require full or
partial payment before the continued performance of services. If you receive an electronic request to change our
banking account number, you should contact our Treasury Department.
(c) Excess use. You must pay additional charges if you exceed the scope of use specified in your order form, based on
the rates specified on the order form or our current standard pricing, whichever is greater.
(d) M&A. The charges remain payable notwithstanding your mergers, acquisitions or divestitures. If you have
enterprise wide or site wide access set out in your order form, our charges are established based on the size of the
company and number of employees as at the date of the order form, and if that materially changes due to a merger,
acquisition or divestiture, we reserve the right to vary the charges.

9. Privacy
Each of us will at all times collect, disclose, store or otherwise process personal data in accordance with applicable
laws relating to the use of personal data relating to individuals ("data privacy laws"), including without limitation any
laws relating to individual rights and cross-border transfers. Each of us will use reasonable efforts to assist one
another in relation to the investigation and remedy of any investigation, claim, allegation, action, suit, proceeding or

CLARIVATE ANALYTICS | TERMS

PAGE 4

litigation with respect to an alleged breach of data privacy laws in relation to activities under the agreement. Each of
us will maintain, and will require any third party data processors to maintain, appropriate physical, technical and
organizational measures to protect the personal data. You may not use personal data included in the services and
products (to the extent such data was not provided by you or collected by us on your behalf) to send bulk or mass
emails or email blasts; to publish or distribute any advertising or promotional material; or to otherwise use such data
in a manner that is prohibited by applicable law. You acknowledge that you are responsible for your own compliance
with data privacy laws, including, where applicable, determining your legal grounds for processing such data. If we
process personal data as a processor on your behalf, the terms of the data processing addendum at
https://clarivate.com/terms-of-business are hereby incorporated by reference. ‘Data controller’, ‘personal data’ and
‘process’ will have the meaning given in the applicable data privacy laws or the data processing addendum.

10. Confidentiality
Each of us will (i) use industry standard administrative, physical and technical safeguards to protect the other’s
confidential information; (ii) only use the confidential information of the other for purposes related to the
performance of the agreement (including our provision of the products); and (ii) not disclose such confidential
information to anyone else except to the extent required by applicable laws or as necessary to perform, manage or
enforce the agreement (including where we need to share it with our subcontractors). If a court or government
agency orders either of us to disclose the confidential information of the other, that party shall notify the other so
that an appropriate protective order or other remedy can be obtained, unless the court or government agency
prohibits prior notification. Confidential information of each party includes any information marked as confidential, or
which a reasonable person would consider as being confidential, including information relating to our property
(including how it is developed and any underlying models or databases) or pricing (but shall not include information
that is or becomes public or known on a non-confidential basis other than through breach of any duty or obligation of
confidence).

11. Audit
(a) Audit right. We or our professional representatives may audit your compliance with the agreement, on at least 10
business days’ notice and during normal business hours, provided that we will not audit more than once in 12
months, unless we reasonably believe you are in breach or we are required to by a third party provider.
(b) Costs. If an audit reveals that you have breached the agreement, you will pay (i) any underpaid charges; and (ii)
the reasonable costs and expenses of undertaking the audit if you have underpaid the charges by more than 5% or if
those costs are imposed on us by a third party provider.

12. Warranties and disclaimers
(a) LIMITED WARRANTY. WE WARRANT THAT (i) WE PROVIDE OUR PRODUCTS USING COMMERCIALLY
REASONABLE SKILL AND CARE AND (ii) OUR INSTALLED SOFTWARE WILL SUBSTANTIALLY CONFORM TO ITS
DOCUMENTATION FOR 90 DAYS AFTER DELIVERY. WE DO NOT OTHERWISE WARRANT UNINTERRUPTED OR
ERROR-FREE OPERATION OF OUR PRODUCTS. TO THE FULLEST EXTENT PERMITTED UNDER APPLICABLE LAWS,
THESE WARRANTIES ARE THE EXCLUSIVE WARRANTIES FROM US AND REPLACE ALL OTHER WARRANTIES,
REPRESENTATIONS AND UNDERTAKINGS, INCLUDING OF PERFORMANCE, MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE, ACCURACY, COMPLETENESS AND CURRENTNESS.
(b) SOFTWARE. IF WE CANNOT RECTIFY ANY VALID SOFTWARE WARRANTY CLAIM WITHIN A REASONABLE PERIOD
YOU MAY CANCEL YOUR LICENSE OF THE AFFECTED SOFTWARE BY WRITTEN NOTICE TO US. WE WILL WITHOUT
ANY FURTHER LIABILITY REFUND ALL APPLICABLE CHARGES.
(c) PROFESSIONAL SERVICES. WE WILL RECTIFY PROFESSIONAL SERVICES IF YOU GIVE US WRITTEN NOTICE OF A
VALID WARRANTY CLAIM WITHIN 30 DAYS OF DELIVERY. IF WE CANNOT RECTIFY ANY VALID WARRANTY CLAIM
WITHIN A REASONABLE PERIOD WE WILL WITHOUT ANY FURTHER LIABILITY REFUND ALL APPLICABLE CHARGES

CLARIVATE ANALYTICS | TERMS

PAGE 5

RELATED TO THE DEFECTIVE SERVICE AND WE MAY TERMINATE THE AFFECTED SERVICES BY WRITTEN NOTICE TO
YOU.
(d) NO ADVICE. WE ARE NOT PROVIDING ANY ADVICE (LEGAL, FINANCIAL OR OTHERWISE) BY ALLOWING YOU TO
ACCESS AND USE OUR PRODUCTS. YOU ARE FULLY RESPONSIBLE FOR YOUR INTERPRETATIONS OF OUR PRODUCTS.
IF YOU DESIRE ADVICE, WE ENCOURAGE YOU TO ENGAGE LEGAL OR FINANCIAL PROFESSIONALS TO HELP YOU
INTERPRET OUR PRODUCTS. YOU ACKNOWLEDGE THAT WE ARE NOT RESPONSIBLE FOR ANY ACTION OR
DAMAGES RESULTING FROM ANY DECISIONS YOU (OR ANY OTHER PARTY ACCESSING THE PRODUCTS THROUGH
YOU) MAKE IN RELIANCE ON OUR PRODUCTS. WE ARE NOT A LAW FIRM OR PROFESSIONAL ADVISOR AND NO
ATTORNEY-CLIENT OR OTHER PROFESSIONAL RELATIONSHIP IS CREATED.
(e) LINKED CONTENT. WE DO NOT ACCEPT ANY RESPONSIBILITY FOR THIRD PARTY CONTENT ACCESSIBLE VIA LINKS
IN OUR PRODUCTS.

13. Liability
(a) Unlimited liabilities. Neither of us excludes or limits liability for (i) fraud, (ii) death or personal injury caused by
negligence, (iii) claims for payment or reimbursement or indemnification or (iv) any other liability, including gross
negligence, where not permitted to do so under applicable laws and nothing in the agreement shall be interpreted to
do so.
(b) Excluded losses. Neither of us will be liable for (i) lost profits, lost business, lost revenue, anticipated savings, lost
data, or lost goodwill; or (ii) any special, incidental or exemplary damages, indirect or consequential losses, or
anticipated savings.
(c) Limitation. The aggregate liability of each of us (and of any of Clarivate’s third party providers) for all claims arising
out of or in connection with the agreement, including for breach of statutory duty, in tort or in negligence (collectively
‘claims’), will not exceed the amount of any actual direct damages up to the amounts payable in the prior 12 months
(or where the claim arose in the first 12 months of the agreement, the amounts that would have been payable in the
first 12 months) for the product that is the subject of the claim.
(d) Claims. You may not assign or transfer claims and you must bring claims within 12 months of arising.
(e) No liability. We will not be responsible for failures or delays that occur because of (i) your or a third party’s
technology or network; (ii) your actions or inaction (other than proper use of the product), such as failing to follow
the usage instructions or adhering to the minimum recommended technical requirements; (iii) changes you make to
our products; (iv) your failure to implement and maintain proper and adequate virus or malware protection and
proper and adequate backup and recovery systems; (v) your failure to install updates we have provided to you; or (vi)
other causes not attributable to us. If we learn that our products failed because of one of these, we reserve the right
to charge you for our work in investigating the failure at our then currently applicable rates. At your request we will
assist you in resolving the failure at a fee to be agreed upon.
(f) Third party intellectual property. If a third party sues you claiming that our products as provided by us infringes
their intellectual property rights, provided your use of our products has been in accordance with the terms of the
agreement, we will defend you against the claim and pay damages that a court finally awards against you or that are
included in a settlement approved by us, provided that you (i) promptly notify us in writing of the claim; (ii) supply
information we reasonably request; and (iii) allow us to control the defense and settlement. We have no liability for
claims to the extent caused by items not provided by us. In relation to liability arising solely from one of our third
party providers' data, software or other materials, our liability will be limited to the amount we recover from that
third party supplier divided by the number of claims by our customers, including you.
(g) Mitigation. Each of us shall take reasonable steps to limit and mitigate any losses, liability, claims or other costs it
may incur under the agreement and which it may seek to recover from the other, including under any
reimbursementor indemnity.
(h) Equitable relief. Each of us agrees that damages may not be a sufficient remedy for any misuse of the others
intellectual property, confidential information or trade secrets, and each of us may seek equitable relief (including
specific performance and injunctive relief) as a remedy for breach of the agreement.

CLARIVATE ANALYTICS | TERMS

PAGE 6

14. Term, Termination
(a) Term. The term and any renewal terms for the products are described in your order form. If either of us does not
wish to renew the products set forth in an order form, in whole or in part, they must provide the other with at least
30 days’ written notice before the end of the then current term.
(b) Suspension. We may on notice suspend or limit your use of our products or other property, or terminate the
agreement, (i) if required to do so by a third party provider, applicable laws, court or regulator; (ii) if you become or
are reasonably likely to become insolvent or affiliated with one of our competitors; or (iii) if there has been or it is
reasonably likely that there will be: a breach of security; a breach of your obligations under the agreement (including
payment); or a violation of third party rights or applicable laws. Our notice will specify the cause of the suspension or
limitation and, as applicable,the actions you must take to reinstate the product. If you do not take the actions or the
cause cannot be remedied within 30 days, we may terminate the agreement. Charges remain payable in full during
periods of suspension or limitation arising from your action or inaction.
(c) Termination. We may terminate the agreement, in whole or in part, in relation to a product which is being
discontinued, on 90 days’ written notice. Either of us may terminate the agreement immediately upon written notice
if the other commits a material breach and (if capable of remedy) fails to cure the material breach within 30 days of
being notified to do so. Unless we terminate for breach or insolvency, pre-paid charges will be refunded on a prorated basis for terminations in accordance with the agreement.
(d) Effect of termination. Except to the extent we have agreed otherwise, upon termination, all your usage rights end
immediately and you must delete or destroy our property and, if requested, confirm this in writing. Termination of
the agreement will not (i) relieve you of your obligation to pay us any amounts you owe up to and including the date
of termination; (ii) affect other accrued rights and obligations; or (iii) terminate those parts of the agreement that by
their nature should continue.

15. Force majeure
Other than payment obligations, neither of us shall be liable for any failure or delay in performance due to causes that
cannot be reasonably controlled by that relevant party, such as (but not limited to) acts of God, acts of any
government, war or other hostility, civil disorder, the elements, fire, explosion, power failure, equipment failure,
industrial or labor dispute, inability to obtain necessary supplies, and the like.

16. Third party rights
Our affiliates and third party providers benefit from our rights and remedies under the agreement. No other third
parties have any rights or remedies under the agreement.

17. General
(a) Assignment. You may not assign or transfer the agreement to anyone else without our prior written consent. We
will provide you with written notice if we assign or transfer the agreement, in whole or in part, as part of our business
reorganization, which we may do provided the products will not be adversely affected.
(b) Marketing. We may refer to you as a customer and use your trade names, trademarks, service marks, logos,
domain names and other brand features in our marketing materials, customer lists, presentations and related
materials.
(c) Amendment. We may amend the agreement from time to time, with such changes being effective upon renewal.
(d) Enforceability. The agreement will always be deemed modified to the minimum extent necessary for it to be
enforceable, unless modification fundamentally changes the agreement.
(e) Non-solicitation. Clarivate is an independent contractor. You must not directly or indirectly employ or engage or
solicit for employment or engagement any personnel of Clarivate during the term and for 12 months thereafter.
Employment resulting from a general public advertisement or search engagement not specifically targeted at the
relevant personnel is not precluded.

CLARIVATE ANALYTICS | TERMS

PAGE 7

(f) Performance. We may perform some or all of our obligations from any of our offices globally or through any of our
affiliates or third parties. Such affiliates and third parties are obligated to confidentiality obligations and we remain
responsible for their performance.
(g) Headings and summaries. Headings and summaries shall not affect the interpretation of the agreement.
(h) Waiver. Neither of us waives our rights or remedies by delay or inaction.
(i) Governing law and jurisdiction. Each of us agrees that any claim arising out of or in connection with the agreement
(including its formation)is subject to the exclusive governing law and exclusive jurisdiction specified in the order form.
(j) Precedence. In the event of any conflict within the agreement, the descending order of precedence is: the order
form; the referenced documents (including any specific product/service terms); the remaining provisions of these
Terms.
(k) Notices. Notices for Clarivate must be directed to contract.admin@clarivate.com. Notices for you will be directed
to the Client entity and address identified in the order form. Each of us may update our notice information upon prior
written notice to the other.
Last updated: May 2021

CLARIVATE ANALYTICS | TERMS

PAGE 8

